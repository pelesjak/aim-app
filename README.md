# AIM-app

## Dependencies
- Allegro lib on Linux
https://github.com/liballeg/allegro_wiki/wiki/Quickstart
- FFTW lib on Unix
http://www.fftw.org/fftw3_doc/Installation-on-Unix.html

## Run
``` bash
make
./main
```
