#include "button.hpp"

ALLEGRO_FONT *Button::font;

bool _count_lines(__attribute__((unused)) int _num_line,
                  __attribute__((unused)) const char *_start,
                  __attribute__((unused)) int _size, void *counter) {
  *(int *)counter += 1;
  return true;
}

void Button::render() {
  al_draw_filled_rectangle(x + 2, y + 2, x + w + 2, y + h + 2, _shadow_color);
  al_draw_filled_rectangle(x, y, x + w, y + h, _bg_color);
  int num_lines = 0;
  al_do_multiline_text(font, w - 10, text->c_str(), &_count_lines, &num_lines);
  al_draw_multiline_text(font, _text_color, x + (w / 2.0),
                         y + ((h - num_lines * line_height) / 2.0), w - 10,
                         line_height, ALLEGRO_ALIGN_CENTRE, text->c_str());
}

void Button::on_click() { _on_click(); }
