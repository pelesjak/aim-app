#include "gui_manager.hpp"

void GUI::render() {
  al_draw_filled_rectangle(0, 0, width, height, al_map_rgb_f(0.71, 1, 1));
  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  al_set_target_bitmap(_buffer);

  al_clear_to_color(al_map_rgba_f(0, 0, 0, 0));
  int min_x = 100000, max_x = -1;
  for (auto &b : buttons) {
    b->render();
    min_x = min(b->x, min_x);
    max_x = max(b->x + b->w, max_x);
  }

  al_set_target_bitmap(old_target);

  _x_offset = (width - (max_x - min_x)) / 2;
  al_draw_scaled_bitmap(_buffer, min_x, 0, max_x - min_x + 2, height, _x_offset,
                        0, max_x - min_x + 2, height, 0);
}

bool GUI::on_click(ALLEGRO_MOUSE_EVENT event) {
  if (event.display != _parent_display)
    return false;
  int x = event.x, y = event.y;
  for (auto &b : buttons) {
    if (b->x + _x_offset <= x && b->x + b->w + _x_offset >= x && b->y <= y &&
        b->y + b->h >= y) {
      b->on_click();
      return true;
    }
  }
  return false;
}

void GUI::update_buffer(int width) {
  if (_buffer) {
    al_destroy_bitmap(_buffer);
  }
  this->width = width;
  _buffer = al_create_bitmap(width, height);
}
