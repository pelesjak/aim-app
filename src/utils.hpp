#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include <cmath>
#include <vector>

double const SQRT_2PI = std::sqrt(2 * M_PI);

inline float gauss_1d(double sigma, double x)
{
  return std::exp(-((x * x) / (2 * sigma * sigma)));
}

inline double gauss_2d(double sigma, double x, double y)
{
  return std::exp(-((x * x + y * y) / (2 * sigma * sigma)));
}

inline std::vector<double> gauss_kernel_1d(double sigma)
{
  int size = std::ceil(2 * sigma);
  std::vector<double> kernel(size * 2 + 1);
  double kernel_sum = 0;
  for (int i = -size; i <= size; i++)
  {
    kernel[i + size] = gauss_1d(sigma, i);
    kernel_sum += kernel[i + size];
  }
  for (int i = 0; i < size * 2 + 1; i++)
  {
    kernel[i] /= kernel_sum;
  }
  return kernel;
}

inline std::vector<double> gauss_kernel_2d(double sigma)
{
  int size = std::ceil(2 * sigma);
  int full_size = size * 2 + 1;
  std::vector<double> kernel(full_size * full_size);
  double kernel_sum = 0;
  for (int i = -size; i <= size; i++)
  {
    for (int j = -size; j <= size; j++)
    {
      kernel[(i + size) * full_size + j + size] = gauss_2d(sigma, i, j);
      kernel_sum += kernel[(i + size) * full_size + j + size];
    }
  }
  for (int i = 0; i < full_size; i++)
  {
    for (int j = 0; j < full_size; j++)
    {
      kernel[i * full_size + j] /= kernel_sum;
    }
  }
  return kernel;
}

const double bilat_bright_diff[16] = {
    0.005,
    0.06666666,
    0.13333333,
    0.2,
    0.26666666,
    0.33333333,
    0.4,
    0.46666666,
    0.3333333,
    0.6,
    0.66666666,
    0.73333333,
    0.8,
    0.86666666,
    0.93333333,
    1};

#endif // __UTILS_HPP__
