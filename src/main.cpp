#include <algorithm>
#include <cstdio>
#include <functional>
#include <iostream>
#include <string>

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/bitmap.h>
#include <allegro5/bitmap_draw.h>
#include <allegro5/color.h>
#include <allegro5/display.h>
#include <allegro5/keycodes.h>

#include "button.hpp"
#include "gui_manager.hpp"
#include "image_manager.hpp"

using namespace std;

int display_w = 1200, display_h = 1000;

struct App_state
{
  ALLEGRO_BITMAP *image_source = nullptr;
  ALLEGRO_BITMAP *image_clone = nullptr;
  float image_scale = 1;
};

void load_image(Image_manager *manager, App_state *state)
{
  ALLEGRO_BITMAP *temp_image = manager->open_image_file(NULL);
  if (temp_image)
  {
    state->image_scale = 1;
    al_destroy_bitmap(state->image_source);
    al_destroy_bitmap(state->image_clone);
    state->image_source = temp_image;
    state->image_clone = al_clone_bitmap(state->image_source);
  }
}

void create_buttons(GUI *gui, Image_manager *manager, App_state *state)
{
  gui->buttons.push_back(
      new Button(0, 0, 100, 50, "Open Image",
                 [manager, state]()
                 { load_image(manager, state); }));
  gui->buttons.push_back(
      new Button(105, 0, 100, 50, "Save Image", [manager, state]()
                 { manager->save_image_file(state->image_clone, NULL); }));

  gui->buttons.push_back(
      new Button(210, 0, 100, 25, "Brightness", [manager, state]() {}));
  gui->buttons.push_back(new Button(
      210, 25, 50, 25, "+", [manager]()
      { manager->delta_brightness(0.1); }));
  gui->buttons.push_back(new Button(
      260, 25, 50, 25, "-", [manager]()
      { manager->delta_brightness(-0.1); }));
  gui->buttons.push_back(new Button(315, 0, 100, 25, "Contrast", []() {}));
  gui->buttons.push_back(new Button(
      315, 25, 50, 25, "+", [manager]()
      { manager->delta_contrast(1.1); }));
  gui->buttons.push_back(new Button(365, 25, 50, 25, "-", [manager]()
                                    { manager->delta_contrast(1.0 / 1.1); }));

  gui->buttons.push_back(new Button(315, 0, 100, 25, "Contrast", []() {}));
  gui->buttons.push_back(new Button(
      315, 25, 50, 25, "+", [manager]()
      { manager->delta_contrast(1.1); }));
  gui->buttons.push_back(new Button(365, 25, 50, 25, "-", [manager]()
                                    { manager->delta_contrast(1.0 / 1.1); }));

  string *monochrome_text =
      new string(manager->_monochrome ? "Monochrome on" : "Monochrome off");
  gui->buttons.push_back(new Button(
      420, 0, 100, 50, monochrome_text, [manager, monochrome_text]()
      {
        manager->toggle_monochrome();
        monochrome_text->assign(manager->_monochrome ? "Monochrome on"
                                                     : "Monochrome off"); }));

  string *amplitude_text = new string(
      manager->_amplitude ? "FFT Amplitude on" : "FFT Amplitude off");
  gui->buttons.push_back(
      new Button(525, 0, 100, 50, amplitude_text, [manager, amplitude_text]()
                 {
        manager->toggle_amplitude();
        amplitude_text->assign(manager->_amplitude ? "FFT Amplitude on"
                                                   : "FFT Amplitude off"); }));

  string *blur_text =
      new string(manager->_blur ? "Gaussian blur on" : "Gaussian blur off");
  gui->buttons.push_back(
      new Button(630, 0, 100, 25, blur_text, [manager, blur_text]()
                 {
        manager->_blur = !manager->_blur;
        blur_text->assign(manager->_blur ? "Gaussian blur on"
                                         : "Gaussian blur off"); }));
  gui->buttons.push_back(new Button(
      630, 25, 25, 25, "1", [manager]()
      { manager->set_gauss_sigma(1); }));
  gui->buttons.push_back(new Button(
      655, 25, 25, 25, "2", [manager]()
      { manager->set_gauss_sigma(2); }));
  gui->buttons.push_back(new Button(
      680, 25, 25, 25, "5", [manager]()
      { manager->set_gauss_sigma(5); }));
  gui->buttons.push_back(new Button(
      705, 25, 25, 25, "10", [manager]()
      { manager->set_gauss_sigma(10); }));

  string *bilateral_text =
      new string(manager->_bilateral_filter ? "Bilateral filter on" : "Bilateral filter off");
  gui->buttons.push_back(
      new Button(735, 0, 100, 25, bilateral_text, [manager, bilateral_text]()
                 {
        manager->_bilateral_filter = !manager->_bilateral_filter;
        bilateral_text->assign(manager->_bilateral_filter ? "Bilateral filter on" : "Bilateral filter off"); }));
  gui->buttons.push_back(new Button(
      735, 25, 25, 25, ".1", [manager]()
      { manager->set_bilateral_sigma(.1); }));
  gui->buttons.push_back(new Button(
      760, 25, 25, 25, ".2", [manager]()
      { manager->set_bilateral_sigma(.2); }));
  gui->buttons.push_back(new Button(
      785, 25, 25, 25, ".5", [manager]()
      { manager->set_bilateral_sigma(.5); }));
  gui->buttons.push_back(new Button(
      810, 25, 25, 25, "1", [manager]()
      { manager->set_bilateral_sigma(1); }));

  gui->buttons.push_back(
      new Button(840, 0, 100, 25, "Combine Image", [manager]()
                 { manager->_combineImage = true; }));
  gui->buttons.push_back(
      new Button(840, 25, 100, 25, "Insert sample", [manager]()
                 { manager->_insertsample = true; }));

  gui->buttons.push_back(
      new Button(945, 0, 50, 25, "Image seg.", [manager]()
                 { 
                   manager->_image_segmentation = true;
                   manager->_sketch_segmentation = false;
                 }));
  gui->buttons.push_back(
      new Button(995, 0, 50, 25, "Sketch seg.", [manager]()
                 { 
                   manager->_image_segmentation = false;
                   manager->_sketch_segmentation = true;
                 }));
  gui->buttons.push_back(new Button(
      945, 25, 25, 25, "2", [manager]()
      { manager->_brush_size = 2; }));
  gui->buttons.push_back(new Button(
      970, 25, 25, 25, "5", [manager]()
      { manager->_brush_size = 5; }));
  gui->buttons.push_back(new Button(
      995, 25, 25, 25, "10", [manager]()
      { manager->_brush_size = 10; }));
  gui->buttons.push_back(new Button(
      1020, 25, 25, 25, "20", [manager]()
      { manager->_brush_size = 20; }));

  gui->buttons.push_back(
      new Button(1050, 0, 100, 50, "Reset", [manager, monochrome_text]()
                 {
        manager->reset();
        monochrome_text->assign("Monochrome off"); }));
}

int main()
{
  al_init();

  ALLEGRO_EVENT_QUEUE *queue = al_create_event_queue();

  ALLEGRO_TIMER *timer = al_create_timer(1.0 / 30.0);
  al_register_event_source(queue, al_get_timer_event_source(timer));

  if (!al_install_mouse())
  {
    cout << "Couldn't initialize mouse controller!" << endl;
    return 1;
  }
  al_register_event_source(queue, al_get_mouse_event_source());

  if (!al_install_keyboard())
  {
    cout << "Couldn't initialize keyboard controller!" << endl;
    return 1;
  }
  al_register_event_source(queue, al_get_keyboard_event_source());

  al_set_new_display_flags(ALLEGRO_RESIZABLE);
  ALLEGRO_DISPLAY *display = al_create_display(display_w, display_h);
  al_set_window_title(display, "Image Editor");
  al_register_event_source(queue, al_get_display_event_source(display));

  if (!al_init_image_addon())
  {
    cout << "Couldn't initialize image addon!" << endl;
    return 1;
  }

  if (!al_init_primitives_addon())
  {
    cout << "Couldn't initialize primitives addon!" << endl;
    return 1;
  }

  al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);

  App_state *app_state = new App_state();

  Image_manager *image_manager = new Image_manager(display);
  GUI *gui_manager = new GUI(display);
  create_buttons(gui_manager, image_manager, app_state);

  app_state->image_source = al_load_bitmap("./images/mysha.png");
  if (!app_state->image_source)
  {
    app_state->image_source = nullptr;
  }
  else
  {
    app_state->image_clone = al_clone_bitmap(app_state->image_source);
  }

  bool redraw = true, done = false;
  float imgX, imgY;
  ALLEGRO_EVENT event;

  int point1X, point1Y, point2X, point2Y;
  bool point1Set = false;
  al_start_timer(timer);
  while (!done)
  {
    al_wait_for_event(queue, &event);

    switch (event.type)
    {

    case ALLEGRO_EVENT_TIMER:
      break;

    case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
      if (event.mouse.button & 1)
      {
        ALLEGRO_MOUSE_EVENT mouse_event = *(ALLEGRO_MOUSE_EVENT *)&event;
        if(image_manager->_image_segmentation || image_manager->_sketch_segmentation){
          if(!point1Set){
            point1X = mouse_event.x - imgX;
            point1Y = mouse_event.y - imgY;
            point1Set = true;
          } else {
            point2X = mouse_event.x - imgX;
            point2Y = mouse_event.y - imgY;
            if(image_manager->_image_segmentation)
              image_manager->segmentation_image(app_state->image_source, point1X, point1Y, point2X, point2Y);
            else {
              vector<int> points(4);
              points[0] = point1X;
              points[1] = point1Y;
              points[2] = point2X;
              points[3] = point2Y;
              image_manager->segmentation_painting(app_state->image_source, points);
            }
            image_manager->_image_segmentation = image_manager->_sketch_segmentation = point1Set = false;
            redraw = true;
          }
        } else if(image_manager->_insertsample){
          image_manager->_insertsample = false;
          image_manager->insert_sample(app_state->image_source, mouse_event.x - imgX, mouse_event.y - imgY);
          redraw = true;
        } else
          redraw = gui_manager->on_click(mouse_event);
      }
      break;

    case ALLEGRO_EVENT_DISPLAY_RESIZE:
      if (display != event.display.source)
        break;
      display_h = event.display.height;
      display_w = event.display.width;
      gui_manager->update_buffer(display_w);
      al_acknowledge_resize(display);
      redraw = true;
      break;

    case ALLEGRO_EVENT_KEY_DOWN:
      switch (event.keyboard.keycode)
      {
      case ALLEGRO_KEY_PAD_PLUS:
        app_state->image_scale = min(5.0, app_state->image_scale + 0.1);
        redraw = true;
        break;
      case ALLEGRO_KEY_MINUS:
      case ALLEGRO_KEY_PAD_MINUS:
        app_state->image_scale = max(0.1, app_state->image_scale - 0.1);
        redraw = true;
        break;
      case ALLEGRO_KEY_O:
        load_image(image_manager, app_state);
        redraw = true;
        break;
      case ALLEGRO_KEY_S:
        image_manager->save_image_file(app_state->image_clone, NULL);
        redraw = true;
        break;
      case ALLEGRO_KEY_R:
        app_state->image_scale = 1.0;
        image_manager->reset();
        redraw = true;
        break;
      case ALLEGRO_KEY_B:
        image_manager->toggle_monochrome();
        redraw = true;
        break;
      case ALLEGRO_KEY_UP:
        image_manager->delta_brightness(0.1);
        redraw = true;
        break;
      case ALLEGRO_KEY_DOWN:
        image_manager->delta_brightness(-0.1);
        redraw = true;
        break;
      case ALLEGRO_KEY_LEFT:
        image_manager->delta_contrast(1.0 / 1.1);
        redraw = true;
        break;
      case ALLEGRO_KEY_RIGHT:
        image_manager->delta_contrast(1.1);
        redraw = true;
        break;
      default:
        break;
      }
      break;

    case ALLEGRO_EVENT_DISPLAY_CLOSE:
      done = true;
      break;
    }

    if (redraw && al_is_event_queue_empty(queue) &&
        app_state->image_source != nullptr)
    {

      image_manager->update_image(app_state->image_source,
                                  app_state->image_clone);

      al_set_target_backbuffer(display);
      al_clear_to_color(al_map_rgb(0, 0, 0));

      gui_manager->render();

      if (app_state->image_clone != nullptr)
      {
        al_set_clipping_rectangle(0, gui_manager->height + 10, display_w,
                                  display_h);
        int w = al_get_bitmap_width(app_state->image_clone),
            h = al_get_bitmap_height(app_state->image_clone);
        float scaled_w = w * app_state->image_scale,
          scaled_h = h * app_state->image_scale;
        imgX = (display_w - scaled_w) / 2.0;
        imgY = (display_h - scaled_h) / 2.0;
        al_draw_scaled_bitmap(
            app_state->image_clone, 0, 0, w, h, imgX, imgY, scaled_w, scaled_h, 0);
        al_set_clipping_rectangle(0, 0, display_w, display_h);
      }

      al_flip_display();

      redraw = false;
    }
  }

  if (app_state->image_source)
  {
    al_destroy_bitmap(app_state->image_source);
  }
  if (app_state->image_clone)
  {
    al_destroy_bitmap(app_state->image_clone);
  }

  al_destroy_display(display);
  al_destroy_timer(timer);
  al_destroy_event_queue(queue);

  return 0;
}
