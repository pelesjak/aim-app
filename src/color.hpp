#ifndef __COLOR_HPP__
#define __COLOR_HPP__

#include <cassert>
#include <iostream>
#include <cmath>

#include <allegro5/allegro5.h>
#include <allegro5/color.h>

class Color
{
public:
  float r, g, b, a;
  Color()
  {
    r = g = b = 0;
    a = 1;
  }
  Color(ALLEGRO_COLOR color)
  {
    r = color.r;
    g = color.g;
    b = color.b;
    a = color.a;
  }
  Color(float grey)
  {
    r = g = b = grey;
    a = 1;
  }
  Color(float r, float g, float b)
  {
    this->r = r;
    this->g = g;
    this->b = b;
    a = 1;
  }
  Color(float r, float g, float b, float a)
  {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
  }
  Color operator+() const;
  Color operator-() const;
  Color &operator+=(const Color &A);
  Color &operator-=(const Color &A);
  Color &operator*=(float x);
  Color &operator/=(float x);
  double getBrightness();
  double getSize();
  double getLength();
  double getMaxLength();
  Color abs();
  Color clamp01();
  Color max(Color &c);
  Color min(Color &c);
  ALLEGRO_COLOR toAllegro();

  friend std::ostream &operator<<(std::ostream &os, const Color &c);
};

inline Color Color::operator+() const { return *this; }

inline Color Color::operator-() const { return Color(-r, -g, -b); }

inline Color &Color::operator+=(const Color &A)
{
  r += A.r;
  g += A.g;
  b += A.b;
  return *this;
}

inline Color &Color::operator-=(const Color &A)
{
  r -= A.r;
  g -= A.g;
  b -= A.b;
  return *this;
}

inline Color &Color::operator*=(float x)
{
  r *= x;
  g *= x;
  b *= x;
  return *this;
}

inline Color &Color::operator/=(float x)
{
  *this *= 1 / x;
  return *this;
}

inline double Color::getBrightness()
{
  return this->r * 0.299 + this->g * 0.587 + this->b * 0.114;
}

inline double Color::getSize()
{
  return std::sqrt(this->r * this->r + this->g * this->g + this->b * this->b) / std::sqrt(3);
}

inline double Color::getLength()
{
  return std::sqrt(this->r * this->r + this->g * this->g + this->b * this->b);
}

inline double Color::getMaxLength()
{
  return std::max(std::max(std::abs(this->r), std::abs(this->g)), std::abs(this->b));
}

inline ALLEGRO_COLOR Color::toAllegro()
{
  return al_map_rgba_f(r, g, b, a);
}

inline Color Color::abs()
{
  return Color(std::abs(this->r), std::abs(this->g), std::abs(this->b));
}

inline Color Color::clamp01()
{
  return Color(std::max(std::min(this->r, 1.0f), 0.0f), std::max(std::min(this->g, 1.0f), 0.0f), std::max(std::min(this->b, 1.0f), 0.0f));
}

inline Color Color::max(Color &c)
{
  return Color(std::max(this->r, c.r), std::max(this->g, c.g), std::max(this->b, c.b));
}

inline Color Color::min(Color &c)
{
  return Color(std::min(this->r, c.r), std::min(this->g, c.g), std::min(this->b, c.b));
}

inline Color operator+(const Color &A, const Color &B)
{
  return Color(A.r + B.r, A.g + B.g, A.b + B.b);
}

inline Color operator-(const Color &A, const Color &B)
{
  return Color(A.r - B.r, A.g - B.g, A.b - B.b);
}

inline Color operator*(Color A, float x)
{
  return Color(A.r * x, A.g * x, A.b * x);
}

inline Color operator*(float x, Color A)
{
  return A * x;
}

inline Color operator*(ALLEGRO_COLOR A, float x)
{
  return Color(A.r * x, A.g * x, A.b * x);
}

inline Color operator*(float x, ALLEGRO_COLOR A)
{
  return A * x;
}

inline std::ostream &operator<<(std::ostream &os, const Color &c)
{
  os << "(" << c.r << ", " << c.g << ", " << c.b << ")";
  return os;
}

#endif // __COLOR_HPP__