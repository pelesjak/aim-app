#ifndef __IMG_MANAGER_HPP__
#define __IMG_MANAGER_HPP__

#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <regex>

#include <allegro5/allegro5.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/bitmap.h>
#include <allegro5/bitmap_lock.h>
#include <allegro5/color.h>

#include <fftw3.h>

#include "color.hpp"
#include "timer.hpp"
#include "utils.hpp"

#include "./GridCut/GridGraph_2D_4C.h"

using namespace std;

#define REAL 0
#define IMAG 1

class Image_manager {
public:
  Image_manager(ALLEGRO_DISPLAY *parent_display) {
    this->_parent_display = parent_display;
  };
  ALLEGRO_BITMAP *open_image_file(const char *init_path);
  bool save_image_file(ALLEGRO_BITMAP *image, const char *init_path);
  void update_image(ALLEGRO_BITMAP *source, ALLEGRO_BITMAP *target);
  void apply_all(ALLEGRO_COLOR &color);
  void apply_brightness(ALLEGRO_COLOR &color, float brightness);
  void apply_contrast(ALLEGRO_COLOR &color, float contrast);
  void to_monochrome(ALLEGRO_COLOR &color, float monochrome_threshold);
  void delta_brightness(float delta);
  void delta_contrast(float delta);
  void toggle_monochrome();
  void delta_monochrome_threshold();
  void reset();

  void toggle_amplitude();
  void amplitude(ALLEGRO_BITMAP *image);

  void set_gauss_sigma(double sigma);
  void gaussian_blur_naive(ALLEGRO_BITMAP *image);
  void gaussian_blur_separable(ALLEGRO_BITMAP *image);

  void set_bilateral_sigma(double sigma);
  void bilateral_filter_naive(ALLEGRO_BITMAP *image);
  void bilateral_filter_piecewise(ALLEGRO_BITMAP *image);

  void combine_images(ALLEGRO_BITMAP *image, const char *filenameA, const char *filenameB);
  void insert_sample(ALLEGRO_BITMAP *image, int posX, int posY);

  void segmentation_image(ALLEGRO_BITMAP *image, int sourceX, int sourceY, int targetX, int targetY);
  void segmentation_painting(ALLEGRO_BITMAP *image, vector<int> points);

  bool _monochrome = false, _amplitude = false, _blur = false, _bilateral_filter = false, _combineImage = false, _insertsample = false, \
    _image_segmentation = false, _sketch_segmentation = false;
  int _brush_size = 10;
  float _brightness = 0, _contrast = 1, _monochrome_threshold = 0.5;

private:
  ALLEGRO_DISPLAY *_parent_display;
  string _last_path;
  ALLEGRO_COLOR _black = al_map_rgb_f(0, 0, 0);
  ALLEGRO_COLOR _white = al_map_rgb_f(1, 1, 1);
  double _gauss_sigma = 1.0;
  vector<double> _gauss_kernel2d = gauss_kernel_2d(_gauss_sigma);
  vector<double> _gauss_kernel1d = gauss_kernel_1d(_gauss_sigma);
  int _gk_size = _gauss_kernel1d.size();
  double _bilat_sigma = 0.4;
};

#endif //__IMG_MANAGER_HPP__
