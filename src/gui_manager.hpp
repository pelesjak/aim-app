#ifndef __GUI_MANAGER_HPP__
#define __GUI_MANAGER_HPP__

#include <vector>

#include <allegro5/allegro5.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/bitmap.h>

#include "button.hpp"

using namespace std;

class GUI {
public:
  const int height = 55;
  int width;
  GUI(ALLEGRO_DISPLAY *display) {
    _parent_display = display;
    width = al_get_display_width(display);
    _buffer = al_create_bitmap(width, height);
  }
  void render();
  bool on_click(ALLEGRO_MOUSE_EVENT click_event);
  void update_buffer(int width);
  vector<Button *> buttons;

private:
  int _x_offset = 0;
  ALLEGRO_BITMAP *_buffer;
  ALLEGRO_DISPLAY *_parent_display;
};

#endif // __GUI_MANAGER_HPP__
