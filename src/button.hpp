#ifndef __BUTTON_HPP__
#define __BUTTON_HPP__

#include <functional>
#include <string>

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>

using namespace std;

class Button {
public:
  static ALLEGRO_FONT *font;
  Button(int x, int y, int w, int h, string text,
         function<void(void)> on_click) {
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
    this->text = new string(text);
    this->_on_click = on_click;

    if (!font) {
      font = al_create_builtin_font();
    }
    line_height = al_get_font_line_height(font);
  };

  Button(int x, int y, int w, int h, string *text,
         function<void(void)> on_click) {
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
    this->text = text;
    this->_on_click = on_click;

    if (!font) {
      font = al_create_builtin_font();
    }
    line_height = al_get_font_line_height(font) + 5;
  };
  Button(int x, int y, int w, int h, string text, function<void(void)> on_click,
         ALLEGRO_COLOR bg_color) {
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
    this->text = new string(text);
    this->_on_click = on_click;
    this->_bg_color = bg_color;
    if (!font) {
      font = al_create_builtin_font();
    }
    line_height = al_get_font_line_height(font) + 5;
  };
  Button(int x, int y, int w, int h, string text, function<void(void)> on_click,
         ALLEGRO_COLOR bg_color, ALLEGRO_COLOR text_color) {
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
    this->text = new string(text);
    this->_on_click = on_click;
    this->_bg_color = bg_color;
    this->_text_color = text_color;
    if (!font) {
      font = al_create_builtin_font();
    }
    line_height = al_get_font_line_height(font) + 5;
  };
  void render();
  void on_click();
  int x, y, w, h;
  int line_height;
  string *text;

private:
  ALLEGRO_COLOR _text_color = al_map_rgb_f(0, 0, 0);
  ALLEGRO_COLOR _bg_color = al_map_rgb_f(0.9, 0.9, 0.9);
  ALLEGRO_COLOR _shadow_color = al_map_rgb_f(0.2, 0.2, 0.2);
  function<void(void)> _on_click;
};

#endif //__BUTTON_HPP__
