#include "image_manager.hpp"

ALLEGRO_BITMAP *Image_manager::open_image_file(const char *init_path)
{
  if (!init_path)
  {
    init_path = _last_path.c_str();
  }

  ALLEGRO_FILECHOOSER *image_browser = al_create_native_file_dialog(
      init_path, "Choose an image", "*.jpg;*.png;*.hdr;",
      ALLEGRO_FILECHOOSER_PICTURES);

  if (!al_show_native_file_dialog(_parent_display, image_browser) ||
      al_get_native_file_dialog_count(image_browser) < 1)
  {
    cout << "No image file was selected!" << endl;
    al_destroy_native_file_dialog(image_browser);
    return nullptr;
  }

  const char *image_path = al_get_native_file_dialog_path(image_browser, 0);
  _last_path = image_path;

  ALLEGRO_BITMAP *image = al_load_bitmap(image_path);

  if (!image)
  {
    cout << "Couldn't load image file! - " << image_path << endl;
    al_destroy_native_file_dialog(image_browser);
    return nullptr;
  }

  al_set_window_title(_parent_display, image_path);

  al_destroy_native_file_dialog(image_browser);
  return image;
}

bool Image_manager::save_image_file(ALLEGRO_BITMAP *image,
                                    const char *init_path)
{
  if (!image)
  {
    cout << "Invalid image object!" << endl;
    return false;
  }

  if (!init_path)
  {
    init_path = _last_path.c_str();
  }

  ALLEGRO_FILECHOOSER *image_browser = al_create_native_file_dialog(
      init_path, "Save an image", "*", ALLEGRO_FILECHOOSER_SAVE);
  ;

  if (!al_show_native_file_dialog(_parent_display, image_browser) ||
      al_get_native_file_dialog_count(image_browser) < 1)
  {
    cout << "No image path was selected!" << endl;
    al_destroy_native_file_dialog(image_browser);
    return false;
  }

  const char *image_path = al_get_native_file_dialog_path(image_browser, 0);
  _last_path = image_path;

  cout << image_path << endl;

  bool ret = al_save_bitmap(image_path, image);

  al_destroy_native_file_dialog(image_browser);

  return ret;
}

void Image_manager::apply_contrast(ALLEGRO_COLOR &color, float contrast)
{
  color.r *= contrast;
  color.g *= contrast;
  color.b *= contrast;
}

void Image_manager::apply_brightness(ALLEGRO_COLOR &color, float brightness)
{
  color.r += brightness;
  color.g += brightness;
  color.b += brightness;
}

void Image_manager::to_monochrome(ALLEGRO_COLOR &color,
                                  float monochrome_threshold)
{
  color = (color.r + color.g + color.b) / 3.0 < monochrome_threshold ? _black
                                                                     : _white;
}

void Image_manager::apply_all(ALLEGRO_COLOR &color)
{
  apply_contrast(color, _contrast);
  apply_brightness(color, _brightness);

  if (_monochrome)
  {
    to_monochrome(color, _monochrome_threshold);
  }
  else
  {
    color.r = min(max((double)color.r, 0.0), 1.0);
    color.g = min(max((double)color.g, 0.0), 1.0);
    color.b = min(max((double)color.b, 0.0), 1.0);
  }
}

void Image_manager::update_image(ALLEGRO_BITMAP *source,
                                 ALLEGRO_BITMAP *target)
{
  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();

  if(_combineImage){
    _combineImage = false;
    if(_last_path.size()){
      if(_last_path.find("over") != string::npos){
        combine_images(source, _last_path.c_str(), regex_replace(_last_path, regex("over"), "under").c_str());
      } else if(_last_path.find("under") != string::npos) {
        combine_images(source, _last_path.c_str(), regex_replace(_last_path, regex("under"), "over").c_str());
      }
    }
  }

  al_set_target_bitmap(target);
  al_lock_bitmap(target, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
  al_lock_bitmap(source, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
  int width = al_get_bitmap_width(source),
      height = al_get_bitmap_height(source);
  // iterating over all pixels of the image
  for (int y = 0; y < height; y++)
  {
    for (int x = 0; x < width; x++)
    {
      ALLEGRO_COLOR color = al_get_pixel(source, x, y);
      // apply all pixel modifications
      apply_all(color);
      al_put_pixel(x, y, color);
    }
  }
  al_unlock_bitmap(source);
  al_unlock_bitmap(target);
  al_set_target_bitmap(old_target);

  if (_bilateral_filter)
  {
    Timer t;
    t.start();
    bilateral_filter_naive(target);
    t.stop();
  }

  if (_blur)
  {
    gaussian_blur_separable(target);
  }

  if (_amplitude)
  {
    amplitude(target);
  }
}

void Image_manager::delta_brightness(float delta)
{
  if (delta > 0)
  {
    _brightness = std::min((float)1.0, _brightness + delta);
  }
  else
  {
    _brightness = std::max((float)-1.0, _brightness + delta);
  }
}
void Image_manager::delta_contrast(float delta)
{
  _contrast = _contrast * delta;
}
void Image_manager::toggle_monochrome() { _monochrome = !_monochrome; }
void Image_manager::delta_monochrome_threshold() {}

void Image_manager::toggle_amplitude() { _amplitude = !_amplitude; }

void Image_manager::reset()
{
  _monochrome = false;
  _amplitude = false;
  _brightness = 0;
  _contrast = 1;
}

double *image_2_array(ALLEGRO_BITMAP *image)
{
  int w = al_get_bitmap_width(image), h = al_get_bitmap_height(image);
  double *arr = new double[w * h];
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
  for (int i = 0; i < h; i++)
  {
    for (int j = 0; j < w; j++)
    {
      arr[w * i + j] = Color(al_get_pixel(image, j, i)).getBrightness();
    }
  }
  al_unlock_bitmap(image);
  return arr;
}

void Image_manager::amplitude(ALLEGRO_BITMAP *image)
{
  if (!image)
    return;
  int w = al_get_bitmap_width(image), h = al_get_bitmap_height(image);
  int half_w = w / 2 + 1, half_h = h / 2;
  double *in = image_2_array(image);
  fftw_complex *out = new fftw_complex[half_w * h];
  fftw_plan p = fftw_plan_dft_r2c_2d(h, w, in, out, FFTW_MEASURE);
  fftw_execute(p);

  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  al_set_target_bitmap(image);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
  Color ampl_col;
  for (int i = 0; i < h; i++)
  {
    for (int j = 0; j < half_w; j++)
    {
      double ampl = sqrt(out[i * half_w + j][REAL] * out[i * half_w + j][REAL] +
                         out[i * half_w + j][IMAG] * out[i * half_w + j][IMAG]);
      ampl = log(ampl + 1) / log(1000);
      ampl = max(0.0, min(1.0, ampl));
      ampl_col = Color(ampl, ampl, ampl);
      al_put_pixel(half_w - 1 - j, half_h - i + (i / (half_h + 1)) * h,
                   ampl_col.toAllegro());
      al_put_pixel(half_w - 1 + j, half_h + i - (i / (h - half_h)) * h,
                   ampl_col.toAllegro());
    }
  }
  al_unlock_bitmap(image);
  al_set_target_bitmap(old_target);

  delete[] in;
  delete[] out;
}

void Image_manager::set_gauss_sigma(double sigma)
{
  _gauss_sigma = sigma;
  _gauss_kernel2d = gauss_kernel_2d(_gauss_sigma);
  _gauss_kernel1d = gauss_kernel_1d(_gauss_sigma);
  _gk_size = _gauss_kernel1d.size();
}

void Image_manager::gaussian_blur_naive(ALLEGRO_BITMAP *image)
{
  if (!image)
    return;

  ALLEGRO_BITMAP *image_copy = al_clone_bitmap(image);
  int w = al_get_bitmap_width(image), h = al_get_bitmap_height(image);
  int center = _gk_size / 2;

  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  al_set_target_bitmap(image);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
  al_lock_bitmap(image_copy, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);

  for (int i = 0; i < h; i++)
  {
    for (int j = 0; j < w; j++)
    {
      Color blured;
      for (int u = -center; u <= center; u++)
      {
        for (int v = -center; v <= center; v++)
        {
          blured += _gauss_kernel2d[(center + v) * _gk_size + (center + u)] *
                    al_get_pixel(image_copy, max(0, min(w - 1, j + u)),
                                 max(0, min(h - 1, i + v)));
        }
      }
      al_put_pixel(j, i, blured.toAllegro());
    }
  }

  al_unlock_bitmap(image);
  al_unlock_bitmap(image_copy);

  al_destroy_bitmap(image_copy);

  al_set_target_bitmap(old_target);
}

void Image_manager::gaussian_blur_separable(ALLEGRO_BITMAP *image)
{
  if (!image)
    return;

  int w = al_get_bitmap_width(image), h = al_get_bitmap_height(image);
  int center = _gk_size / 2;

  ALLEGRO_BITMAP *temp_image = al_create_bitmap(w, h);

  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  al_set_target_bitmap(temp_image);
  al_lock_bitmap(temp_image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
  for (int i = 0; i < h; i++)
  {
    for (int j = 0; j < w; j++)
    {
      Color blured;
      for (int u = -center; u <= center; u++)
      {
        int x = j + u;
        x = x < 0 || x >= w ? j - u : x;
        blured += _gauss_kernel1d[center + u] * al_get_pixel(image, x, i);
      }
      al_put_pixel(j, i, blured.toAllegro());
    }
  }

  al_unlock_bitmap(image);
  al_unlock_bitmap(temp_image);

  al_set_target_bitmap(image);
  al_lock_bitmap(temp_image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);

  for (int j = 0; j < w; j++)
  {
    for (int i = 0; i < h; i++)
    {
      Color blured;
      for (int v = -center; v <= center; v++)
      {
        int y = i + v;
        y = y < 0 || y >= h ? i - v : y;
        blured += _gauss_kernel1d[center + v] * al_get_pixel(temp_image, j, y);
      }
      al_put_pixel(j, i, blured.toAllegro());
    }
  }

  al_unlock_bitmap(image);
  al_unlock_bitmap(temp_image);

  al_destroy_bitmap(temp_image);

  al_set_target_bitmap(old_target);
}

void Image_manager::set_bilateral_sigma(double sigma)
{
  _bilat_sigma = sigma;
}

void Image_manager::bilateral_filter_naive(ALLEGRO_BITMAP *image)
{
  if (!image)
    return;

  ALLEGRO_BITMAP *image_copy = al_clone_bitmap(image);
  int w = al_get_bitmap_width(image), h = al_get_bitmap_height(image);
  int center = _gk_size / 2;

  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  al_set_target_bitmap(image);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
  al_lock_bitmap(image_copy, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);

  for (int i = 0; i < h; i++)
  {
    for (int j = 0; j < w; j++)
    {
      Color blured;
      double currentSize = Color(al_get_pixel(image_copy, j, i)).getSize();
      float norm = 0;
      for (int u = -center; u <= center; u++)
      {
        for (int v = -center; v <= center; v++)
        {
          Color pix = al_get_pixel(image_copy, max(0, min(w - 1, j + u)),
                                   max(0, min(h - 1, i + v)));
          float koef = _gauss_kernel2d[(center + v) * _gk_size + (center + u)] * gauss_1d(_bilat_sigma, log(pix.getSize() / currentSize));
          blured += koef * pix;
          norm += koef;
        }
      }
      blured /= norm;
      al_put_pixel(j, i, blured.toAllegro());
    }
  }

  al_unlock_bitmap(image);
  al_unlock_bitmap(image_copy);

  al_destroy_bitmap(image_copy);

  al_set_target_bitmap(old_target);
}

void Image_manager::bilateral_filter_piecewise(ALLEGRO_BITMAP *image)
{
  if (!image)
    return;

  int w = al_get_bitmap_width(image), h = al_get_bitmap_height(image);

  float *simimilarity_image[16];
  for (int k = 0; k < 16; k++)
  {
    simimilarity_image[k] = new float[w * h];
  }

  ALLEGRO_BITMAP *temp_image = al_create_bitmap(w * 4, h * 4);

  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READWRITE);
  al_lock_bitmap(temp_image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READWRITE);
  for (int i = 0; i < h; i++)
  {
    for (int j = 0; j < w; j++)
    {
      Color pix = al_get_pixel(image, j, i);
      double size = pix.getSize();
      for (int k = 0; k < 16; k++)
      {
        float sym = gauss_1d(_bilat_sigma, log(size / bilat_bright_diff[k]));
        simimilarity_image[k][i * w + j] = sym;
      }
    }
  }

  int center = _gk_size / 2;

  al_set_target_bitmap(temp_image);
  for (int i = 0; i < h; i++)
  {
    for (int j = 0; j < w; j++)
    {
      for (int k = 0; k < 16; k++)
      {
        Color blured;
        float sim_sum = 0;
        for (int u = -center; u <= center; u++)
        {
          int x = j + u;
          x = x < 0 || x >= w ? j - u : x;
          float similarity = simimilarity_image[k][i * w + x] * _gauss_kernel1d[center + u];
          sim_sum += similarity;
          blured += similarity * al_get_pixel(image, x, i);
        }
        blured /= sim_sum;
        al_put_pixel(j + w * (k % 4), i + h * (k / 4), blured.toAllegro());
      }
    }
  }
  al_set_target_bitmap(image);
  for (int j = 0; j < w; j++)
  {
    for (int i = 0; i < h; i++)
    {
      Color pix = al_get_pixel(image, j, i);
      double bright = pix.getSize() / 0.06666666;
      int l = bright;
      int m = l + 1;
      Color finals[2];
      for (int k = l; k <= m; k++)
      {
        Color blured;
        float sim_sum = 0;
        for (int v = -center; v <= center; v++)
        {
          int y = i + v;
          y = y < 0 || y >= h ? i - v : y;
          float similarity = simimilarity_image[k][y * w + j] * _gauss_kernel1d[center + v];
          sim_sum += similarity;
          blured += similarity * al_get_pixel(temp_image, j + w * (k % 4), y + h * (k / 4));
        }
        blured /= sim_sum;
        finals[k - l] = blured;
      }
      al_put_pixel(j, i, Color((m - bright) * finals[0] + (bright - l) * finals[1]).toAllegro());
    }
  }

  al_unlock_bitmap(image);
  al_unlock_bitmap(temp_image);
  al_destroy_bitmap(temp_image);

  for (int k = 0; k < 16; k++)
  {
    delete simimilarity_image[k];
  }

  al_set_target_bitmap(old_target);
}

void Image_manager::combine_images(ALLEGRO_BITMAP *image, const char *filenameA, const char *filenameB)
{
  if (!image)
    return;

  cout << "Combining images: " << filenameA << " and " << filenameB << endl;

  ALLEGRO_BITMAP* over = al_load_bitmap(filenameA);
  ALLEGRO_BITMAP* under = al_load_bitmap(filenameB);
  int w = al_get_bitmap_width(over), h = al_get_bitmap_height(over);
  Color *derX = new Color[w * h];
  Color *derY = new Color[w * h];
  Color *divergence = new Color[w * h];
  Color *blend = new Color[w * h];
  Color *combine = new Color[w * h];
  al_lock_bitmap(over, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
  al_lock_bitmap(under, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
  Color pix_over, prevX_over, prevY_over, derX_over, derY_over;
  Color pix_under, prevX_under, prevY_under, derX_under, derY_under;
  for (int j = h - 1; j >= 0; j--)
  {
    for (int i = w - 1; i >= 0; i--)
    {
      pix_over = al_get_pixel(over, i, j);
      pix_under = al_get_pixel(under, i, j);
      blend[j * w + i] = combine[j * w + i] = 0.5 * pix_over + 0.5 * pix_under;
      derX_over = i != 0 ? al_get_pixel(over, i - 1, j) - pix_over : derX[j * w + i + 1];
      derX_under = i != 0 ? al_get_pixel(under, i - 1, j) - pix_under : derX[j * w + i + 1];
      derY_over = j != 0 ? al_get_pixel(over, i, j - 1) - pix_over : derY[(j + 1) * w + i];
      derY_under = j != 0 ? al_get_pixel(under, i, j - 1) - pix_under : derY[(j + 1) * w + i];
      derX[j * w + i] = derX_over.getLength() > derX_under.getLength() ? derX_over : derX_under;
      derY[j * w + i] = derY_over.getLength() > derY_under.getLength() ? derY_over : derY_under;
    }
  }
  al_unlock_bitmap(over);
  al_unlock_bitmap(under);
  for (int j = 0; j < h; j++)
  {
    int y = j + 1 < h ? j + 1 : j;
    for (int i = 0; i < w; i++)
    {
      int x = i + 1 < w ? i + 1 : i;
      divergence[j * w + i] = derX[j * w + i] - derX[j * w + x] + derY[j * w + i] - derY[y * w + i];
    }
  }

  Timer t;
  t.start();
  cout << "Start Gauss-Seidel" << endl;
  int counter = 0;
  Color xplus, xminus, yplus, yminus;
  while (true)
  {
    double update = 0;
    for (int j = 0; j < h; j++)
    {
      for (int i = 0; i < w; i++)
      {
        xplus = i + 1 < w ? combine[j * w + i + 1] : blend[j * w + i];
        xminus = i != 0 ? combine[j * w + i - 1] : blend[j * w + i];
        yplus = j + 1 < h ? combine[(j + 1) * w + i] : blend[j * w + i];
        yminus = j != 0 ? combine[(j - 1) * w + i] : blend[j * w + i];
        Color pix = (xplus + xminus + yplus + yminus - divergence[j * w + i]) * 0.25;
        update = max(update, (combine[j * w + i] - pix).getLength());
        combine[j * w + i] = pix;
      }
    }
    if(counter % 10 == 0){
      cout << update << endl;
    }
    if(update < 0.0005){
      break;
    }
    counter++;
  }

  Color minC = Color(1000), maxC = Color(-1000);
  for (int j = 0; j < h; j++)
  {
    for (int i = 0; i < w; i++)
    {
      minC = minC.min(combine[j * w + i]);
      maxC = maxC.max(combine[j * w + i]);
    }
  }
  
  double minI = min(min(minC.r, minC.g), minC.b);
  double maxI = min(min(maxC.r, maxC.g), maxC.b);

  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  al_set_target_bitmap(image);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
  
  for (int j = 0; j < h; j++)
  {
    for (int i = 0; i < w; i++)
    {
      al_put_pixel(i, j, ((combine[j * w + i] - Color(minI)) * (1 /(maxI - minI))).toAllegro());
    }
  }
  t.stop();
  cout << "End Gauss-Seidel" << " Time: "<< t.get_secs() << "Iterations: " << counter << endl;
  al_unlock_bitmap(image);
  delete[] derX;
  delete[] derY;
  delete[] divergence;
  delete[] combine;
  delete[] blend;
  al_destroy_bitmap(over);
  al_destroy_bitmap(under);
  al_set_target_bitmap(old_target);
}

void Image_manager::insert_sample(ALLEGRO_BITMAP *image, int posX, int posY)
{
  if (!image)
    return;
  
  ALLEGRO_BITMAP* sample = al_load_bitmap("./images/eye.png");
  int w = al_get_bitmap_width(sample), h = al_get_bitmap_height(sample);

  int offsetX = min(al_get_bitmap_width(image) - w, max(0, posX - w/2));
  int offsetY = min(al_get_bitmap_height(image) - h, max(0, posY - h/2));

  cout << "Insering sample at: (" << posX << ", "<< posY << ")" << endl;
  Color *derX = new Color[w * h];
  Color *derY = new Color[w * h];
  Color *divergence = new Color[w * h];
  Color *combine = new Color[w * h];
  al_lock_bitmap(sample, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
  Color pix, prevX, prevY;
  for (int j = 0; j < h; j++)
  {
    for (int i = 0; i < w; i++)
    {
      pix = al_get_pixel(sample, i, j);
      derX[j * w + i] = i != 0 ? al_get_pixel(sample, i - 1, j) - pix : 0;
      derY[j * w + i] = j != 0 ? al_get_pixel(sample, i, j - 1) - pix : 0;
      combine[j * w + i] = 0;
    }
  }
  
  for (int j = 0; j < h; j++)
  {
    int y = j + 1 < h ? j + 1 : j;
    for (int i = 0; i < w; i++)
    {
      int x = i + 1 < w ? i + 1 : i;
      divergence[j * w + i] = derX[j * w + i] - derX[j * w + x] + derY[j * w + i] - derY[y * w + i];
      if(i == 0)
        divergence[j * w + i] -= al_get_pixel(image, offsetX - 1, j + offsetY);
      if(i == w - 1)
        divergence[j * w + i] -= al_get_pixel(image, offsetX + w, j + offsetY);
      if(j == 0)
        divergence[j * w + i] -= al_get_pixel(image, offsetX + i, offsetY - 1);
      if(j == h - 1)
        divergence[j * w + i] -= al_get_pixel(image, offsetX + i, offsetY + h);
    }
  }
  al_unlock_bitmap(sample);
  al_unlock_bitmap(image);

  Timer t;
  t.start();
  cout << "Start Gauss-Seidel" << endl;
  int counter = 0;
  Color adjust;
  while (true)
  {
    double update = 0;
    for (int j = 0; j < h; j++)
    {
      for (int i = 0; i < w; i++)
      {
        adjust = 0;
        adjust += i + 1 < w ? combine[j * w + i + 1] : 0;
        adjust += i != 0 ? combine[j * w + i - 1] : 0;
        adjust += j + 1 < h ? combine[(j + 1) * w + i] : 0;
        adjust += j != 0 ? combine[(j - 1) * w + i] : 0;
        Color pix = (adjust - divergence[j * w + i]) * 0.25;
        update = max(update, (combine[j * w + i] - pix).getLength());
        combine[j * w + i] = pix;
      }
    }
    if(counter % 100 == 0){
      cout << update << endl;
    }
    if(update < 0.0001){
      break;
    }
    counter++;
  }

  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  al_set_target_bitmap(image);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
  
  for (int j = 0; j < h; j++)
  {
    for (int i = 0; i < w; i++)
    {
      al_put_pixel(i+offsetX, j+offsetY, combine[j * w + i].clamp01().toAllegro());
    }
  }
  t.stop();
  cout << "End Gauss-Seidel" << " Time: "<< t.get_secs() << " Iterations: " << counter << endl;
  al_unlock_bitmap(image);

  al_set_target_bitmap(old_target);

  delete[] derX;
  delete[] derY;
  delete[] divergence;
  delete[] combine;
  al_destroy_bitmap(sample);
}

float sigma_capacity = 64;
int compute_capacity_image(Color A, Color B) {
  Color t = ((A - B) * 255);
  return (1.0 / (1 + (t.r*t.r + t.g*t.g + t.b*t.b) / sigma_capacity)) * 255.0;
}

bool is_in_circle(double cX, double cY, double pX, double pY, double r) {
  return pow(cX - pX,2) + pow(cY - pY, 2) <= pow(r, 2);
}

typedef GridGraph_2D_4C<int, int, int> Graph;
void Image_manager::segmentation_image(ALLEGRO_BITMAP *image, int sourceX, int sourceY, int targetX, int targetY)
{
  int r = _brush_size;
  float lambda = 2.0 / r;
  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  
  int w = al_get_bitmap_width(image), h = al_get_bitmap_height(image);

  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READWRITE);

  Graph* graph = new Graph(w, h);
  if(graph->bad_alloc()){
    return;
  }

  float sourceHistogram[16][16][16];
  float targetHistogram[16][16][16];
  int counterTerminal = 0, counterSource = 0;
  for(int j = 0; j < h; j++) {
    for(int i = 0; i < w; i++) {
      if(is_in_circle(sourceX, sourceY, i, j, r)) {
        Color c = al_get_pixel(image, i, j);
        sourceHistogram[(int)(c.r*15)][(int)(c.g*15)][(int)(c.b*15)]++;
        counterSource++;
      } else if (is_in_circle(targetX, targetY, i, j, r)){
        Color c = al_get_pixel(image, i, j);
        targetHistogram[(int)(c.r*15)][(int)(c.g*15)][(int)(c.b*15)]++;
        counterTerminal++;
      }
    }
  }

  for(int a = 0; a < 16; a++) {
    for(int b = 0; b < 16; b++) {
      for(int c = 0; c < 16; c++) {
        sourceHistogram[a][b][c] /= counterSource;
        targetHistogram[a][b][c] /= counterTerminal;
      }
    }
  }

  int cap, totalCapacity;
  for(int j = 0; j < h; j++) {
    for(int i = 0; i < w; i++) {
      int id = graph->node_id(i,j);
      Color c = al_get_pixel(image, i, j);
      totalCapacity = 1;
      if(j - 1 >= 0) {
        cap = compute_capacity_image(c, al_get_pixel(image, i, j - 1));
        totalCapacity += cap;
        graph->set_neighbor_cap(id, 0, -1, cap);
      }
      if(j + 1 < h) {
        cap = compute_capacity_image(c, al_get_pixel(image, i, j + 1));
        totalCapacity += cap;
        graph->set_neighbor_cap(id, 0, 1, cap);
      }
      if(i - 1 >= 0) {
        cap = compute_capacity_image(c, al_get_pixel(image, i - 1, j));
        totalCapacity += cap;
        graph->set_neighbor_cap(id, -1, 0, cap);
      }
      if(i + 1 < w) {
        cap = compute_capacity_image(c, al_get_pixel(image, i + 1, j));
        totalCapacity += cap;
        graph->set_neighbor_cap(id, 1, 0, cap);
      }
      
      if(is_in_circle(sourceX, sourceY, i, j, r)) {
        graph->set_terminal_cap(id, totalCapacity*lambda, 0);
      } else if (is_in_circle(targetX, targetY, i, j, r)){
        graph->set_terminal_cap(id, 0, totalCapacity*lambda);
      } else {
        float scale = totalCapacity*lambda;
        float probSource = sourceHistogram[(int)(c.r*15)][(int)(c.g*15)][(int)(c.b*15)];
        float probTarget = targetHistogram[(int)(c.r*15)][(int)(c.g*15)][(int)(c.b*15)];
        graph->set_terminal_cap(id, scale*probSource, scale*probTarget);
      }
    }
  }
  al_unlock_bitmap(image);

  graph->compute_maxflow();

  al_set_target_bitmap(image);
  for(int j = 0; j < h; j++) {
    for(int i = 0; i < w; i++) {
      int segment = graph->get_segment(graph->node_id(i,j));

      if(segment){
        al_draw_pixel(i, j, al_map_rgba_f(1,0,0, 0.3));
      }

      if(is_in_circle(sourceX, sourceY, i, j, r)) {
        al_draw_pixel(i, j, al_map_rgba_f(0,1,0, 0.1));
      } else if (is_in_circle(targetX, targetY, i, j, r)){
        al_draw_pixel(i, j, al_map_rgba_f(0,0,1, 0.1));
      }
    }
  }
  

  al_set_target_bitmap(old_target);
}

int compute_capacity_painting(Color A, Color B) {
  return 1 + pow(min(A.getBrightness(), B.getBrightness()), 1.8)*255;
}

struct pixel_info {
  double grey;
  int mark = -1;
  int label = -1;
};
void Image_manager::segmentation_painting(ALLEGRO_BITMAP *image, vector<int> points)
{
  int r = _brush_size;
  float lambda = 2.0 / r;
  ALLEGRO_BITMAP *old_target = al_get_target_bitmap();
  
  int w = al_get_bitmap_width(image), h = al_get_bitmap_height(image);
  al_lock_bitmap(image, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READWRITE);

  pixel_info *data = new pixel_info[w*h];
  for(int j = 0; j < h; j++) {
    for(int i = 0; i < w; i++) {
      data[j*w + i].grey = Color(al_get_pixel(image, i, j)).getBrightness();
      if(i <= 2 || j <= 2 || i >= w - 3 || j >= h - 3){
        data[j*w + i].mark = 0;
      }
      for(int k = 0; k < (int)points.size()/2; k++){
        if(is_in_circle(points[k*2], points[k*2 + 1], i, j, r)) {
          data[j*w + i].mark = k+1;
        }
      }
    }
  }

  int sourceIndex = 0;
  while (true) {
    Graph* graph = new Graph(w, h);
    if(graph->bad_alloc()){
      return;
    }
    int cap, totalCapacity;
    for(int j = 0; j < h; j++) {
      for(int i = 0; i < w; i++) {
        if(data[j*w + i].label != -1) continue;
        int id = graph->node_id(i,j);
        Color c = al_get_pixel(image, i, j);
        totalCapacity = 1;
        if(j - 1 >= 0) {
          cap = compute_capacity_painting(c, al_get_pixel(image, i, j - 1));
          totalCapacity += cap;
          graph->set_neighbor_cap(id, 0, -1, cap);
        }
        if(j + 1 < h) {
          cap = compute_capacity_painting(c, al_get_pixel(image, i, j + 1));
          totalCapacity += cap;
          graph->set_neighbor_cap(id, 0, 1, cap);
        }
        if(i - 1 >= 0) {
          cap = compute_capacity_painting(c, al_get_pixel(image, i - 1, j));
          totalCapacity += cap;
          graph->set_neighbor_cap(id, -1, 0, cap);
        }
        if(i + 1 < w) {
          cap = compute_capacity_painting(c, al_get_pixel(image, i + 1, j));
          totalCapacity += cap;
          graph->set_neighbor_cap(id, 1, 0, cap);
        }
        
        if(data[j*w + i].mark == sourceIndex) {
          graph->set_terminal_cap(id, totalCapacity*lambda, 0);
        } else if (data[j*w + i].mark != -1 && data[j*w + i].mark != sourceIndex){
          graph->set_terminal_cap(id, 0, totalCapacity*lambda);
        } else {
          graph->set_terminal_cap(id, 0, 0);
        }
      }
    }
    graph->compute_maxflow();

    bool allLabeled = true;
    for(int j = 0; j < h; j++) {
      for(int i = 0; i < w; i++) {
        if(data[j*w + i].label != -1) continue;
        int segment = graph->get_segment(graph->node_id(i,j));
        if(segment == 0){
          data[j*w + i].label = sourceIndex;
        } else {
          allLabeled = false;
        }
      }
    }
    sourceIndex = (sourceIndex+1)%(points.size()/2 + 1);
    if(allLabeled) break;
  }

  al_unlock_bitmap(image);

  al_set_target_bitmap(image);
  for(int j = 0; j < h; j++) {
    for(int i = 0; i < w; i++) {
      int l = data[j*w + i].label;
      if(!l) continue;
      Color c = (l+1)%3==0 ? Color(196.0/255, 70.0/255, 45.0/255) : Color(214.0/255, 96.0/255, 210.0/255);
      c *= data[j*w + i].grey;
      if(data[j*w + i].mark != -1) {
        c = Color(1,0,0, 0.3);
      }
      al_draw_pixel(i, j, c.toAllegro());
    }
  }

  al_set_target_bitmap(old_target);
}
