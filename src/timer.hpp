#ifndef __TIMER_HPP__
#define __TIMER_HPP__

#include <chrono>


class Timer {
public:
  void start();
  void stop() ;
  double get_secs();
  double get_mili();
  double get_micro();
  double get_nano();
private:
  std::chrono::time_point<std::chrono::system_clock> _start_time;
  std::chrono::time_point<std::chrono::system_clock> _end_time;
};

#endif // __TIMER_HPP__
