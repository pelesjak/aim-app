#include "timer.hpp"

void Timer::start() { _start_time = std::chrono::system_clock::now(); }

void Timer::stop() { _end_time = std::chrono::system_clock::now(); }

double Timer::get_nano() {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(_end_time -
                                                              _start_time)
      .count();
}

double Timer::get_micro() {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(_end_time -
                                                              _start_time)
             .count() /
         1000.0;
}

double Timer::get_mili() {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(_end_time -
                                                              _start_time)
             .count() /
         1000000.0;
}

double Timer::get_secs() {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(_end_time -
                                                              _start_time)
             .count() /
         1000000000.0;
}
